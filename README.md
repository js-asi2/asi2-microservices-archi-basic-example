# ASI2-Microservices-Archi-basic-example

Exemple simple d'une architecture microservice (hors reverse proxy).
- 1 microservice A
- 1 microservice B

## Fonctionnement:

Pour créer l'objet ACollection le micro service a besoin d'information du microservice B (/random) et d'une API extérieur (https://api.chucknorris.io/jokes/random)
le micro Serivce appelle ces 2 API via RestTemplate (requète HTTP)

## Image d'architecture

<img src="./img/MicroServices-Basic-Example.png" alt="Targeted Architecture" width="600"/>


## Usage

- Créer une image via Micro B (e.g postman, insomnia)

    - HTTP METHOD: `POST`
    - URL:         `localhost:8081/bimg` 
    - Body: `{"title": "myImg", "tagList": ["sport","economy"],	"imgURL": " https://www.premiere.fr/sites/default/files/styles/scale_crop_1280x720/public/2019-10/ghost-in-the-shell-sac-2045-1174708-1280x0.jpeg" }`


- Créer une collection via le Micro A (e.g postman, insomnia)
  - HTTP METHOD: `POST`
  - URL:         `localhost:8080/acollection`
  - Body: `{"title": "mycollection", "description":"myDescription" }`
  

- Récupérer les collections afin de vérifier le bon fonctionnement
  - HTTP METHOD: `GET`
  - URL:         `localhost:8080/acollection`



