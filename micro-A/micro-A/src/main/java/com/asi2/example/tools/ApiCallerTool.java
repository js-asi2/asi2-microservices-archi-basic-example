package com.asi2.example.tools;


import io.netty.handler.logging.LogLevel;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.client.RestTemplate;

import com.asi2.example.a.model.ChuckApiModel;
import com.asi2.example.b.model.BImgDTO;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

public class ApiCallerTool {
	public final static String B_IMG_URL_BASE="http://localhost:8081";
	public final static String B_IMG_URL_ACTION_RANDOM="/random";
	public final static String CHUCK_URL_BASE="https://api.chucknorris.io/jokes/random";
	
	
	public static BImgDTO callBImgGetRandom() {
		// Create the Web client
		WebClient client = WebClient.builder()
				.baseUrl(B_IMG_URL_BASE + B_IMG_URL_ACTION_RANDOM)
				.defaultCookie("cookieKey", "cookieValue")
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.build();

		try {
			BImgDTO bImgResponse = client.get()
					.retrieve()
					.bodyToMono(BImgDTO.class).block();
			return bImgResponse;
		}catch (WebClientResponseException ex){
			System.out.println("error to remote API:"+ex.getStatusText());
		}
		return null;
	}

	public static ChuckApiModel callChuckApi() {

		// Use to log request through Webclient
		HttpClient httpClient = HttpClient.create()
				.wiretap("callChuckApi", LogLevel.INFO, AdvancedByteBufFormat.TEXTUAL);
		ClientHttpConnector conn = new ReactorClientHttpConnector(httpClient);

		// Create the Web client
		WebClient client = WebClient.builder()
				.clientConnector(conn)
				.baseUrl(CHUCK_URL_BASE)
				.defaultCookie("cookieKey", "cookieValue")
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.build();

		try {
			ChuckApiModel chuckResponse = client.get()
					.retrieve()
					.bodyToMono(ChuckApiModel.class).block();
			return chuckResponse;
		}catch (WebClientResponseException ex){
			System.out.println("error to remote API:"+ex.getStatusText());
		}
			return null;
	}

}
