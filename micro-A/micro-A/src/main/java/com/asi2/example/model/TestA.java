package com.asi2.example.model;


import java.io.Serializable;


public class TestA implements Serializable {
    private String name;

    public TestA() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
